package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class InformationServlet extends HttpServlet {

	private static final long serialVersionUID = 8880012107939109054L;
	
	// Database placeholder for storing information
	private ArrayList<String> data;
	
	/*
	* Creating, Initializing and Finalizing Servlets
	* The "init" method is used to indicate to a servlet that the servlet is being placed into service.
	* Can be used to initialize values for use within the servlet and other
	* necessary functions when using a servlet.
	*/
	public void init() throws ServletException {
		// Initializes the servlet with the following data
		data = new ArrayList<>(Arrays.asList("Standard", "Deluxe"));
		System.out.println("******************************************");
		System.out.println(" InformationServlet has been initialized. ");
		System.out.println("******************************************");
	}
	
	/*
	* Service methods that can be used several times during the lifetime of
	* a servlet to handle requests and responses.
	*/
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
		/*
		* Sharing information via servletContext
		* Retrieves all context information provided in the web.xml file.
		*/
		ServletContext srvContext = getServletContext();
		String hotelName = srvContext.getInitParameter("hotel_name");
		String hotelAddress = srvContext.getInitParameter("hotel_address");
		String hotelContact = srvContext.getInitParameter("hotel_contact");
		
		// Data passed via ServletConfig
		ServletConfig servConfig = getServletConfig();
		String status = servConfig.getInitParameter("status");		
		
		PrintWriter out = res.getWriter();
		
		// Data from the "RoomServlet" created from the "doGet" method
		// String facilities = System.getProperty("facilities");
		
		// Data passed via the send redirect
		String facilities = req.getParameter("facilities");
		
		// Data passed via the HttpSession
		HttpSession session = req.getSession();
		String roomsAvailable = session.getAttribute("availableRooms").toString();
		
		/*
		* The information will only be available when the "doGet" method from the RoomServlet
		* is accessed, else the value will show null
		*/
		out.println(
			"<h1>Hotel Information</h1>" +
			"<p>Hotel Name: " + hotelName + "</p>" +
			"<p>Hotel Address: " + hotelAddress + "</p>" +
			"<p>Hotel Contact: " + hotelContact + "</p>" +
			"<p>Rooms Status: " + status + "</p>" +
			"<p>Hotel Facilities: " + facilities + "</p>" +
			"<p>Rooms Available: " + roomsAvailable + "</p>"
			
		);
		
	}
	
	/*
	* Unlike a "GET" request which can send data via a query string, a 
	* "doPost" method sends the information only through the request object
	* and provides for a cleaner url.
	*/
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String roomName = req.getParameter("roomtype");
		
		data.add(roomName);
		
		PrintWriter out = res.getWriter();
		
		out.println(data);
	}
	
	/*
	* Java Servlets have other service methods to handle other common requests
	* like delete and update.
	* HTML can only process "GET" and "POST" requests but other frontend
	* technologies can and "DELETE" and "PUT" requests are other requests 
	* that are commonly processed.
	*/
	public void doDelete(HttpServletRequest req, HttpServletResponse res) throws IOException {
		data.remove(1);
		
		PrintWriter out = res.getWriter();
		
		out.println(data);
	}
	
	public void doPut(HttpServletRequest req, HttpServletResponse res) throws IOException {
		data.set(1, "Jane");
		
		PrintWriter out = res.getWriter();
		
		out.println(data);
	}
	
	/*
	* The "destroy" method is used to indicate to a servlet that it is being
	* taken out of service and perform functions other functions that need
	* to be executed at the end of a servlet's lifecycle
	*/
	public void destroy() {
		System.out.println("****************************************");
		System.out.println(" InformationServlet has been destroyed. ");
		System.out.println("****************************************");
	}

}
