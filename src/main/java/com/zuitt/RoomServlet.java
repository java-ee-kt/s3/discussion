package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class RoomServlet extends HttpServlet {

	private static final long serialVersionUID = 7802515668491130811L;
	
	// Database placeholder for storing information
	private ArrayList<String> data = new ArrayList<>();

	public void init() throws ServletException {
		System.out.println("***********************************");
		System.out.println(" RoomServlet has been initialized. ");
		System.out.println("***********************************");
	}
	
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		// Sharing data via the System Properties
		System.getProperties().put("facilities", "Swimming Pool,Gym, Grand Ballroom, Offices");
		String facilities = System.getProperty("facilities");
		PrintWriter out = res.getWriter();
		
		out.println(facilities);
		
		/*
		* Session Management
		* Sending information by rewriting the url of the information class.
		* This also redirects the servlet to the information servlet and passing the data via
		* the query string.
		*/
		// Sending information via HttpSession
		HttpSession session = req.getSession();
		session.setAttribute("availableRooms", "standard");
		
		res.sendRedirect("information?facilities="+facilities);
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		ServletContext srvContext = getServletContext();
		
		String roomName = req.getParameter("roomtype");
		
		data.add(roomName);
		
		srvContext.setAttribute("data", data);
		
		/*
		*  The request dispatcher will forward the request and response bodies to the
		*  information servlet and invoke the "doPost" method.
		*  Since the request is a post request, this also triggers the post request from the
		*  information servlet.
		*  The data printed using the printWriter will no longer be shown.
		*/
		RequestDispatcher rd = req.getRequestDispatcher("information");
		rd.forward(req, res);
		
		PrintWriter out = res.getWriter();
		
		out.println(data);
	}
	
	public void destroy() {
		System.out.println("********************************");
		System.out.println(" RoomServlet has been destroyed. ");
		System.out.println("********************************");
	}
	
}
